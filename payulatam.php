<?php 

defined ('_JEXEC') or die('Restricted Access');

/**
 * @version $Id: payulatam.php, v 1.0 2014/04/07
 *
 * PayuLatam payment plugin for VirtueMart
 * @author Gabriel Martinez
 * @package VirtueMart
 * @subpackage payment
 * @copyright Copyright (C) 2014 Payu Latam - All rights reserved
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
 *
 * http://virtuemart.net
 */

if (!class_exists ('vmPSPlugin')) {
	require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');
}

class plgVmPaymentPayulatam extends vmPSPlugin {
	
	function __construct(& $subject, $config) {

    parent::__construct($subject, $config);

		$this->_loggable = TRUE;

		$this->tableFields = array_keys($this->getTableSQLFields());

		$this->_tablepkey = 'id';

		$this->_tableId = 'id';

		$varsToPush = $this->getVarsToPush();

		$this->setConfigParameterable($this->_configTableFieldName, $varsToPush);

	}

	/**
   * Create the table for this plugin if it does not yet exist.
   *
   * @author Valérie Isaksen
   */
  public function getVmPluginCreateTableSQL () {

    return $this->createTableSQL('Payment PayuLatam Table');

  }
	
	/**
   * Fields to create the payment table
   *
   * @return string SQL Fileds
   */
  function getTableSQLFields () {

    $SQLfields = array(
      'id'													=> 'int(1) UNSIGNED NOT NULL AUTO_INCREMENT',
      'virtuemart_order_id'					=> 'int(1) UNSIGNED',
      'order_number'								=> 'char(64)',
      'virtuemart_paymentmethod_id'	=> 'mediumint(1) UNSIGNED',
      'payment_name'								=> 'varchar(5000)',
      'payment_order_total'					=> 'decimal(15,5) NOT NULL DEFAULT \'0.00000\'',
      'payment_currency'						=> 'char(3)',
      'email_currency'							=> 'char(3)',
      'cost_per_transaction'				=> 'decimal(10,2)',
      'cost_percent_total'					=> 'decimal(10,2)',
      'tax_id'											=> 'smallint(1)',
      'merchant_id'									=> 'varchar(20)',
      'transaction_state'						=> 'smallint',
			'risk'												=> 'decimal(10,2)',
			'pol_response_code'						=> 'char(64)',
			'reference_code'							=> 'char(50)',
			'reference_pol'								=> 'char(255)',
			'signature'										=> 'char(255)',
			'pol_payment_method'					=> 'char(255)',
			'pol_payment_method_type'			=> 'smallint',
			'installments_number'					=> 'smallint',
			'tx_value'										=> 'decimal(14,2)',
			'tx_tax'											=> 'decimal(14,2)',
			'buyer_email'									=> 'char(255)',
			'processing_date'							=> 'char(28)',
			'currency'										=> 'char(3)',
			'cus'													=> 'char(255)',
			'pse_bank'										=> 'char(255)',
			'lng'													=> 'char(2)',
			'description'									=> 'char(255)',
			'lap_response_code'						=> 'char(64)',
			'lap_payment_method'					=> 'char(255)',
			'lap_payment_method_type'			=> 'char(255)',
			'lap_transaction_state'				=> 'char(32)',
			'message'											=> 'char(255)',
			'extra1'											=> 'char(255)',
			'extra2'											=> 'char(255)',
			'extra3'											=> 'char(255)',
			'authorization_code'					=> 'char(12)',
			'merchant_address'						=> 'char(255)',
			'merchant_name'								=> 'char(255)',
			'merchant_url'								=> 'char(255)',
			'order_language'							=> 'char(2)',
			'pse_cycle'										=> 'varchar(20)',
			'pse_reference1'							=> 'char(255)',
			'pse_reference2'							=> 'char(255)',
			'pse_reference3'							=> 'char(255)',
			'telephone'										=> 'char(20)',
			'transaction_id'							=> 'char(36)',
			'trazability_code'						=> 'char(64)',
			'tx_administrative_fee'				=> 'decimal(10,2)',
			'tx_tax_administrative_fee'		=> 'decimal(10,2)',
			'tx_tax_administrative_fee_return_base'	=> 'decimal(10,2)',
			'action_code_description'			=> 'char(255)',
			'cc_holder'										=> 'char(150)',
			'cc_number'										=> 'char(255)',
			'processing_date_time'				=> 'char(28)',
			'request_number'							=> 'char(9)',
			'nickname_buyer'							=> 'char(150)',
			'nickname_seller'							=> 'char(150)',
			'phone'												=> 'char(20)',
			'office_phone'								=> 'char(20)',
			'payment_request_state'				=> 'char(32)',
			'test'												=> 'smallint',
			'airline_code'								=> 'char(4)',
			'ip'													=> 'char(39)',
			'attempts'										=> 'smallint',
			'additional_value'						=> 'decimal(14,2)',
			'error_message_bank'					=> 'char(255)',
			'error_code_bank'							=> 'char(255)',
			'billing_address'							=> 'char(255)',
    	'billing_city' 								=> 'char(255)',
    	'billing_country' 						=> 'char(2)',
    	'shipping_address' 						=> 'char(50)',
			'shipping_city' 							=> 'char(50)',
    	'shipping_country' 						=> 'char(2)',
			'commision_pol' 							=> 'decimal(10,2)',
			'commision_pol_currency' 			=> 'char(3)',
			'transaction_bank_id' 				=> 'char(255)'
    );

    return $SQLfields;
  }

	function _getHtmlForm($method, $order, $cart) {

		$baseUri = JURI::base() . "plugins/vmpayment/payulatam/";

		$totalInPaymentCurrency = 
			vmPSPlugin::getAmountInCurrency($order['details']['BT']->order_total,$method->payment_currency);
		$amount = $totalInPaymentCurrency['value'];
		$referenceCode = $order['details']['BT']->order_number;
		$currency = shopFunctions::getCurrencyByID($method->payment_currency, 'currency_code_3'); 

		$iva = floatval($cart->pricesUnformatted['taxAmount']);
    $baseIva = 0;

    if($iva != 0) {
      $baseIva = floatval($cart->pricesUnformatted['basePrice']);
    }

		// set response url
		$payMethod = $order['details']['BT']->virtuemart_paymentmethod_id;

		$responseUrl = JURI::base() . "index.php?option=com_virtuemart&amp;" .
			"view=pluginresponse&amp;task=pluginresponsereceived&amp;paymethod=" . $payMethod;
			
		$confirmationUrl = JURI::base() . "index.php?option=com_virtuemart&amp;" .
			"view=pluginresponse&amp;task=pluginnotification";
			
		// shipping data
		$shippingAddress = $order['details']['ST']->address_1;
		$shippingCity = $order['details']['ST']->city;
		$shippingCountry = ShopFunctions::getCountryByID($order['details']['ST']->virtuemart_country_id, 'country_2_code');

		if(!isset($shippingAddress) || $shippingAddress == null || $shippingAddress == '') {
			$shippingAddress = $order['details']['BT']->address_1;
			$shippingCity = $order['details']['BT']->city;
			$shippingCountry = ShopFunctions::getCountryByID($order['details']['BT']->virtuemart_country_id, 'country_2_code');
		}

		// set post fields
		$payInfo = array (
			"merchantId"			=> $method->merchant_id,
			"accountId"				=> $method->account_id,
			"referenceCode" 	=> $referenceCode,
			"description"   	=> "sale ". $referenceCode,
			"amount"					=> $amount,
			"currency"				=> $currency,
			"tax"							=> $iva,
			"test"						=> $method->use_test,
			"taxReturnBase" 	=> $baseIva,
			"signature"				=> md5($method->api_key."~".$method->merchant_id."~".$referenceCode."~".$amount."~".$currency),
			"buyerEmail"			=> $order['details']['BT']->email,
			"responseUrl"			=> $responseUrl,
			"confirmationUrl"	=> $confirmationUrl,
			"extra1"					=> "VIRTUEMART",
			"billingAddress" 	=> $order['details']['BT']->address_1,
      "billingCity"     => $order['details']['BT']->city,
      "billingCountry" 	=> ShopFunctions::getCountryByID($order['details']['BT']->virtuemart_country_id, 'country_2_code'),
      "shippingAddress" => $shippingAddress,
      "shippingCity"    => $shippingCity,
      "shippingCountry" => $shippingCountry
		);

		// set post fields
		$payInfo = array (
			"merchantId" 		=> $method->merchant_id,
    	"accountId" 		=> $method->account_id,
    	"referenceCode" => $referenceCode,
    	"description" 	=> "sale ". $referenceCode,
    	"amount" 				=> $amount,
    	"currency" 			=> $currency,
    	"tax" 					=> $iva,
			"test"					=> $method->use_test,
    	"taxReturnBase" => $baseIva,
    	"signature" 		=> md5($method->api_key."~".$method->merchant_id."~".$referenceCode."~".$amount."~".$currency),
    	"buyerEmail" 		=> $order['details']['BT']->email,
			"responseUrl"		=> $responseUrl,
			"confirmationUrl"		=> $confirmationUrl,
			"extra1"				=> "VIRTUEMART",
			"billingAddress" => $order['details']['BT']->address_1,
      "billingCity" 	=> $order['details']['BT']->city,
      "billingCountry" => ShopFunctions::getCountryByID($order['details']['BT']->virtuemart_country_id, 'country_2_code'),
      "shippingAddress" => $order['details']['ST']->address_1,
      "shippingCity" 	=> $order['details']['ST']->city,
      "shippingCountry" => ShopFunctions::getCountryByID($order['details']['ST']->virtuemart_country_id, 'country_2_code')
		);

		// get url
		$url = $method->url_produccion;
		
		if($method->use_test == 1) {
			$url = $method->url_pruebas;
		}

		// create form
		$html = '<html><head><title>Redirect</title></head>';
		$html .= '<body><form action="'.$url.'" method="post" name="payulatam_form" accept-charset="UTF-8">';

		foreach($payInfo as $key => $value) {
			$html .= '<input type="hidden" name="'.$key.'" value="'.htmlspecialchars($value).'" />';
		}

		$html .= '<input type="submit" value="' . JText::_('VMPAYMENT_PAYULATAM_REDIRECT') . '" />';

		$html .= '</form>';

		$html .= '<script type="text/javascript" > document.payulatam_form.submit(); </script>';

		$html .= '</body></html>';

		return $html;
		
	}

	/**
   *
   * @author Valérie Isaksen
	 * @author Gabriel Martinez
   */
  function plgVmConfirmedOrder ($cart, $order) {


    if (!($method = $this->getVmPluginMethod ($order['details']['BT']->virtuemart_paymentmethod_id))) {
      return NULL; // Another method was selected, do nothing
    }

    if (!$this->selectedThisElement ($method->payment_element)) {
      return FALSE;
    }

    $lang = JFactory::getLanguage ();
    $filename = 'com_virtuemart';
    $lang->load ($filename, JPATH_ADMINISTRATOR);

    if (!class_exists ('VirtueMartModelOrders')) {
      require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php');
    }

    $this->getPaymentCurrency($method);

		$currency_code_3 = shopFunctions::getCurrencyByID($method->payment_currency, 'currency_code_3');

		$email_currency = $this->getEmailCurrency($method);

    $totalInPaymentCurrency = vmPSPlugin::getAmountInCurrency($order['details']['BT']->order_total,$method->payment_currency);

    $dbValues['payment_name'] = $this->renderPluginName ($method, $order);
    $dbValues['order_number'] = $order['details']['BT']->order_number;
    $dbValues['virtuemart_paymentmethod_id'] = $order['details']['BT']->virtuemart_paymentmethod_id;
    $dbValues['cost_per_transaction'] = $method->cost_per_transaction;
    $dbValues['cost_percent_total'] = $method->cost_percent_total;
    $dbValues['payment_currency'] = $currency_code_3;
    $dbValues['email_currency'] = $email_currency;
    $dbValues['payment_order_total'] = $totalInPaymentCurrency['value'];
    $dbValues['tax_id'] = $method->tax_id;
 		$this->storePSPluginInternalData ($dbValues);

		$html = $this->_getHtmlForm($method, $order, $cart);

		$cart->_confirmDone = FALSE;
		$cart->_dataValidated = FALSE;
		$cart->setCartIntoSession();

		/*vmdebug('HTML', htmlspecialchars($html));
		return NULL;*/

		JRequest::setVar('html', $html);

    return TRUE;
  }

	/*
   * Keep backwards compatibility
   * a new parameter has been added in the xml file
   */
  function getNewStatus ($method) {

    if (isset($method->status_pending) and $method->status_pending!="") {
      return $method->status_pending;
    } else {
      return 'P';
    }
  }

	/**
   * Display stored payment data for an order
   *
   */
  function plgVmOnShowOrderBEPayment ($virtuemart_order_id, $virtuemart_payment_id) {

    if (!$this->selectedThisByMethodId ($virtuemart_payment_id)) {
      return NULL; // Another method was selected, do nothing
    }

    if (!($paymentTable = $this->getDataByOrderId ($virtuemart_order_id))) {
      return NULL;
    }

    VmConfig::loadJLang('com_virtuemart');

    $html = '<table class="adminlist">' . "\n";
    $html .= $this->getHtmlHeaderBE ();
    $html .= $this->getHtmlRowBE ('COM_VIRTUEMART_PAYMENT_NAME', $paymentTable->payment_name);
    $html .= $this->getHtmlRowBE ('STANDARD_PAYMENT_TOTAL_CURRENCY', 
				$paymentTable->payment_order_total . ' ' . $paymentTable->payment_currency);

    if ($paymentTable->email_currency) {
      $html .= $this->getHtmlRowBE ('STANDARD_EMAIL_CURRENCY', $paymentTable->email_currency );
    }

    $html .= '</table>' . "\n";

    return $html;
  }

	/**
   * Check if the payment conditions are fulfilled for this payment method
   *
   * @author: Valerie Isaksen
   *
   * @param $cart_prices: cart prices
   * @param $payment
   * @return true: if the conditions are fulfilled, false otherwise
   *
   */
  protected function checkConditions ($cart, $method, $cart_prices) {

    $this->convert_condition_amount($method);
    $amount = $this->getCartAmount($cart_prices);
    $address = (($cart->ST == 0) ? $cart->BT : $cart->ST);

    //vmdebug('standard checkConditions',  $amount, $cart_prices['salesPrice'],  $cart_prices['salesPriceCoupon']);
    $amount_cond = ($amount >= $method->min_amount AND $amount <= $method->max_amount OR
      	($method->min_amount <= $amount AND ($method->max_amount == 0)));

    if (!$amount_cond) {
      return FALSE;
    }

    $countries = array();

    if (!empty($method->countries)) {

      if (!is_array ($method->countries)) {

        $countries[0] = $method->countries;

      } else {

        $countries = $method->countries;

      }

    }

    // probably did not gave his BT:ST address
    if (!is_array ($address)) {
      $address = array();
      $address['virtuemart_country_id'] = 0;
    }
		
		if (!isset($address['virtuemart_country_id'])) {
      $address['virtuemart_country_id'] = 0;
    }

    if (count ($countries) == 0 || in_array ($address['virtuemart_country_id'], $countries) ) {
      return TRUE;
    }

    return FALSE;
  }

	/**
   * Create the table for this plugin if it does not yet exist.
   * This functions checks if the called plugin is active one.
   * When yes it is calling the standard method to create the tables
   *
   * @author Valérie Isaksen
   *
   */
  function plgVmOnStoreInstallPaymentPluginTable ($jplugin_id) {

    return $this->onStoreInstallPluginTable ($jplugin_id);
  }

	/**
   * This event is fired after the payment method has been selected. It can be used to store
   * additional payment info in the cart.
   *
   * @author Max Milbers
   * @author Valérie isaksen
   *
   * @param VirtueMartCart $cart: the actual cart
   * @return null if the payment was not selected, true if the data is valid, error message if the data is not vlaid
   *
   */
  public function plgVmOnSelectCheckPayment (VirtueMartCart $cart, &$msg) {

    return $this->OnSelectCheck ($cart);
  }

	/**
   * plgVmDisplayListFEPayment
   * This event is fired to display the pluginmethods in the cart (edit shipment/payment) for exampel
   *
   * @param object  $cart Cart object
   * @param integer $selected ID of the method selected
   * @return boolean True on succes, false on failures, null when this plugin was not selected.
   * On errors, JError::raiseWarning (or JError::raiseError) must be used to set a message.
   *
   * @author Valerie Isaksen
   * @author Max Milbers
   */
  public function plgVmDisplayListFEPayment (VirtueMartCart $cart, $selected = 0, &$htmlIn) {

    return $this->displayListFE ($cart, $selected, $htmlIn);
  }

	/*
	 * plgVmonSelectedCalculatePricePayment
	 * Calculate the price (value, tax_id) of the selected method
	 * It is called by the calculator
	 * This function does NOT to be reimplemented. If not reimplemented, then the default values from this function are taken.
	 * @author Valerie Isaksen
	 * @cart: VirtueMartCart the current cart
	 * @cart_prices: array the new cart prices
	 * @return null if the method was not selected, false if the shiiping rate is not valid any more, true otherwise
	 *
	 *
	 */
	public function plgVmonSelectedCalculatePricePayment (VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {
    return $this->onSelectedCalculatePrice ($cart, $cart_prices, $cart_prices_name);
  }

	function plgVmgetPaymentCurrency ($virtuemart_paymentmethod_id, &$paymentCurrencyId) {

    if (!($method = $this->getVmPluginMethod ($virtuemart_paymentmethod_id))) {
      return NULL; // Another method was selected, do nothing
    }

    if (!$this->selectedThisElement ($method->payment_element)) {
      return FALSE;
    }

    $this->getPaymentCurrency ($method);

    $paymentCurrencyId = $method->payment_currency;

    return;

  }

	/**
   * plgVmOnCheckAutomaticSelectedPayment
   * Checks how many plugins are available. If only one, the user will not have the choice. Enter edit_xxx page
   * The plugin must check first if it is the correct type
   *
   * @author Valerie Isaksen
   * @param VirtueMartCart cart: the cart object
   * @return null if no plugin was found, 0 if more then one plugin was found,  virtuemart_xxx_id if only one plugin is found
   *
   */
  function plgVmOnCheckAutomaticSelectedPayment (VirtueMartCart $cart, array $cart_prices = array(), &$paymentCounter) {

    return $this->onCheckAutomaticSelected ($cart, $cart_prices, $paymentCounter);

  }

	/**
   * This method is fired when showing the order details in the frontend.
   * It displays the method-specific data.
   *
   * @param integer $order_id The order ID
   * @return mixed Null for methods that aren't active, text (HTML) otherwise
   * @author Max Milbers
   * @author Valerie Isaksen
   */
  public function plgVmOnShowOrderFEPayment ($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name) {

    $this->onShowOrderFE ($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
  }

	/**
   * @param $orderDetails
   * @param $data
   * @return null
   */
  function plgVmOnUserInvoice ($orderDetails, &$data) {

    if (!($method = $this->getVmPluginMethod ($orderDetails['virtuemart_paymentmethod_id']))) {
      return NULL; // Another method was selected, do nothing
    }

    if (!$this->selectedThisElement ($method->payment_element)) {
      return NULL;
    }

    //vmdebug('plgVmOnUserInvoice',$orderDetails, $method);

    if (!isset($method->send_invoice_on_order_null) or 
				$method->send_invoice_on_order_null==1 or $orderDetails['order_total'] > 0.00){

      return NULL;

    }

    if ($orderDetails['order_salesPrice']==0.00) {
      $data['invoice_number'] = 'reservedByPayment_' . $orderDetails['order_number']; // Nerver send the invoice via email
    }

  }

	/**
    * @param $virtuemart_paymentmethod_id
    * @param $paymentCurrencyId
    * @return bool|null
    */
	function plgVmgetEmailCurrency($virtuemart_paymentmethod_id, $virtuemart_order_id, &$emailCurrencyId) {

		if (!($method = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
   		return NULL; // Another method was selected, do nothing
    }

    if (!$this->selectedThisElement($method->payment_element)) {
     return FALSE;
    }

    if (!($payments = $this->getDatasByOrderId($virtuemart_order_id))) {
      // JError::raiseWarning(500, $db->getErrorMsg());
      return '';
    }

    if (empty($payments[0]->email_currency)) {

      $vendorId = 1; //VirtueMartModelVendor::getLoggedVendor();
      $db = JFactory::getDBO();
      $q = 'SELECT   `vendor_currency` FROM `#__virtuemart_vendors` WHERE `virtuemart_vendor_id`=' . $vendorId;
      $db->setQuery($q);
      $emailCurrencyId = $db->loadResult();

    } else {

      $emailCurrencyId = $payments[0]->email_currency;

    }

  }

	/**
   * This event is fired during the checkout process. It can be used to validate the
   * method data as entered by the user.
   *
   * @return boolean True when the data was valid, false otherwise. If the plugin is not activated, it should return null.
   * @author Max Milbers
	public function plgVmOnCheckoutCheckDataPayment(  VirtueMartCart $cart) {

  	return null;

  }

  /**
   * This method is fired when showing when priting an Order
   * It displays the the payment method-specific data.
   *
   * @param integer $_virtuemart_order_id The order ID
   * @param integer $method_id  method used for this order
   * @return mixed Null when for payment methods that were not selected, text (HTML) otherwise
   * @author Valerie Isaksen
   */
  function plgVmonShowOrderPrintPayment ($order_number, $method_id) {
    return $this->onShowOrderPrint ($order_number, $method_id);
  }

  function plgVmDeclarePluginParamsPayment ($name, $id, &$data) {
    return $this->declarePluginParams ('payment', $name, $id, $data);
  }

  function plgVmSetOnTablePluginParamsPayment ($name, $id, &$table) {
    return $this->setOnTablePluginParams ($name, $id, $table);
  }

	/**
   * Save updated order data to the method specific table
   *
   * @param array   $_formData Form data
   * @return mixed, True on success, false on failures (the rest of the save-process will be
   * skipped!), or null when this method is not actived.
   *
  public function plgVmOnUpdateOrderPayment(  $_formData) {
  	return null;
  }

  /**
   * Save updated orderline data to the method specific table
   *
   * @param array   $_formData Form data
   * @return mixed, True on success, false on failures (the rest of the save-process will be
   * skipped!), or null when this method is not actived.
   *
  public function plgVmOnUpdateOrderLine(  $_formData) {
  	return null;
  }

  /**
   * plgVmOnEditOrderLineBE
   * This method is fired when editing the order line details in the backend.
   * It can be used to add line specific package codes
   *
   * @param integer $_orderId The order ID
   * @param integer $_lineId
   * @return mixed Null for method that aren't active, text (HTML) otherwise
   *
  public function plgVmOnEditOrderLineBEPayment(  $_orderId, $_lineId) {
  	return null;
  }

	/**
   * This method is fired when showing the order details in the frontend, for every orderline.
   * It can be used to display line specific package codes, e.g. with a link to external tracking and
   * tracing systems
   *
   * @param integer $_orderId The order ID
   * @param integer $_lineId
   * @return mixed Null for method that aren't active, text (HTML) otherwise
   *
  public function plgVmOnShowOrderLineFE(  $_orderId, $_lineId) {
  	return null;
  }

 /**
   * This event is fired when the  method notifies you when an event occurs that affects the order.
   * Typically,  the events  represents for payment authorizations, Fraud Management Filter actions and other actions,
   * such as refunds, disputes, and chargebacks.
   *
   * NOTE for Plugin developers:
   *  If the plugin is NOT actually executed (not the selected payment method), this method must return NULL
   *
   * @param         $return_context: it was given and sent in the payment form. The notification should return it back.
   * Used to know which cart should be emptied, in case it is still in the session.
   * @param int     $virtuemart_order_id : payment  order id
   * @param char    $new_status : new_status for this order id.
   * @return mixed Null when this method was not selected, otherwise the true or false
   *
   * @author Valerie Isaksen
   *
   *
  public function plgVmOnPaymentNotification() {
  	return null;
  }

	/**
   * plgVmOnPaymentResponseReceived
   *
	 * @author Gabriel Martinez
   **/
  function plgVmOnPaymentResponseReceived(&$html) {

		if (!class_exists('VirtueMartCart')) {
      require(JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php');
    }
    if (!class_exists('shopFunctionsF')) {
      require(JPATH_VM_SITE . DS . 'helpers' . DS . 'shopfunctionsf.php');
    }
    if (!class_exists('VirtueMartModelOrders')) {
      require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php');
    }
    VmConfig::loadJLang('com_virtuemart_orders',TRUE);
		
		$values = $this->_getResponsePayuValues();
		
    $vendorId = 0;
		$payMethod = $_REQUEST['paymethod'];

    if (!($this->_currentMethod = $this->getVmPluginMethod($payMethod))) {
      return NULL; // Another method was selected, do nothing
    }

    if (!$this->selectedThisElement($this->_currentMethod->payment_element)) {
      return NULL;
    }

    if (!($virtuemartOrderId = VirtueMartModelOrders::getOrderIdByOrderNumber($values['referenceCode']))) {
      return NULL;
		}

		$paymentName = $this->renderPluginName($this->_currentMethod);

		VmConfig::loadJLang('com_virtuemart');
    $orderModel = VmModel::getModel('orders');
    $order = $orderModel->getOrder($virtuemartOrderId);

		if (!class_exists('CurrencyDisplay'))
      require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'currencydisplay.php');

    $currency = CurrencyDisplay::getInstance('',$order['details']['BT']->virtuemart_vendor_id);
		
		$success = $values['transactionState'] == '4' || $values['transactionState'] == '7';

		if(!isset($success)) $success = false;

		$html = $this->renderByLayout('response', array("success" => $success,
	                                                                                        "payment_name" => $paymentName,
                                                                                          "order" => $order,
																																													"values" => $values,
                                                                                          "currency" => $currency,
                                                                                     ));

		$cart = VirtueMartCart::getCart();
    $cart->emptyCart();
    return TRUE;

  }
  
	/**
   *
	 * @author Gabriel Martinez
   **/
  public function plgVmOnPaymentNotification() {
		if (!class_exists('VirtueMartModelOrders')) {
      require(JPATH_VM_ADMINISTRATOR . DS . 'models' . DS . 'orders.php');
    }
		$values = $this->_getConfirmationPayuValues();
		
		$orderId = $values['reference_sale'];
		
		if (!($virtuemartOrderId = VirtueMartModelOrders::getOrderIdByOrderNumber($orderId))) {
      return NULL;
		}
		
		if(!($payment = $this->getDataByOrderId($virtuemartOrderId)) ) {
			return FALSE;
		}
		
		$this->_currentMethod = $this->getVmPluginMethod($payment->virtuemart_paymentmethod_id);
		if (!$this->selectedThisElement($this->_currentMethod->payment_element) ) {
			return FALSE;
		}
		
		$orderModel = VmModel::getModel('orders');
		
		$order = $orderModel->getOrder($virtuemartOrderId);
		
		$result = $this->_validatePayment($values);
		
		if(!$result) {
			return FALSE;
		} else {
			
			$payuData = $this->_getPayuInternalData($values);
			
			$payuData['order_number'] = $values['reference_sale'];
			$payuData['virtuemart_order_id'] = $virtuemartOrderId;
			$payuData['virtuemart_paymentmethod_id'] = $payment->virtuemart_paymentmethod_id;
			
			$this->storePSPluginInternalData ($payuData, 'id', 0);
			
			// This raw method is used because call storePSPluginInternalData insert a new row instead update
			//$this->_storePluginDataToDb($payuData, $payment->id);
			
			$orderModel->updateStatusForOneOrder($virtuemartOrderId, $result, TRUE);
			
		}
		
		return true;
  }
	
	function _storePluginDataToDb($payuData, $id) {
		require_once(JPATH_SITE . '/configuration.php');
		
		$config = new JConfig();
		
		$host = $config->host;
		
		$user = $config->user;
		
		$password = $config->password;
		
		$dataBase = $config->db;
		
		$conn = mysql_connect($host, $user, $password);
		
		if(!$conn) {
			vmdebug('error', 'Error connecting to db in raw db method');
		}
		
		if(!mysql_select_db($dataBase, $conn)) {
			vmdebug('error', 'Error selecting the db');
    }
		
		$query = "UPDATE j_virtuemart_payment_plg_payulatam set ";
		
		foreach($payuData as $key => $value) {
			$query .= $key . " = '" . mysql_real_escape_string(htmlentities($value)) . "' , ";
		}
		
		$query .= "extra2 = '" . mysql_real_escape_string(htmlentities($payuData['extra2'])) . "'";
		$query .= " where id = '" . $id . "'";
		
		$result = mysql_query($query);
		
		if (!$result) {
			vmdebug('error', 'Error updating plugin payment table');
		}
	}
	
	function _getPayuInternalData($values) {
		$data = array();
		
		$data['merchant_id'] = $values['merchant_id'];
		$data['transaction_state'] = $values['state_pol'];
		$data['risk'] = $values['risk'];
		$data['pol_response_code'] = $values['response_code_pol'];
		$data['reference_code'] = $values['reference_sale'];
		$data['reference_pol'] = $values['reference_pol'];
		$data['signature'] = $values['sign'];
		$data['pol_payment_method'] = $values['payment_method'];
		$data['pol_payment_method_type'] = $values['payment_method_type'];
		$data['installments_number'] = $values['installments_number'];
		$data['tx_value'] = $values['value'];
		$data['tx_tax'] = $values['tax'];
		$data['buyer_email'] = $values['email_buyer'];
		$data['processing_date'] = $values['transaction_date'];
		$data['currency'] = $values['currency'];
		$data['cus'] = $values['cus'];
		$data['pse_bank'] = $values['pse_bank'];
		$data['description'] = $values['description'];
		$data['message'] = $values['response_message_pol'];
		$data['extra1'] = $values['extra1'];
		$data['extra2'] = $values['extra2'];
		$data['authorization_code'] = $values['authorization_code'];
		$data['pse_reference1'] = $values['pse_reference1'];
		$data['pse_reference2'] = $values['pse_reference2'];
		$data['pse_reference3'] = $values['pse_reference3'];
		$data['phone'] = $values['phone'];
		$data['office_phone'] = $values['office_phone'];
		$data['transaction_id'] = $values['transaction_id'];
		$data['tx_administrative_fee'] = $values['administrative_fee'];
		$data['tx_tax_administrative_fee'] = $values['administrative_fee_tax'];
		$data['tx_tax_administrative_fee_return_base'] = $values['administrative_fee_base'];
		$data['processing_date_time'] = $values['date'];
		$data['request_number'] = $values['transaction_id'];
		$data['nickname_buyer'] = $values['nickname_buyer'];
		$data['nickname_seller'] = $values['nickname_seller'];
		$data['payment_request_state'] = $values['payment_request_state'];
		$data['test'] = $values['test'];
		$data['airline_code'] = $values['airline_code'];
		$data['ip'] = $values['ip'];
		$data['attempts'] = $values['attempts'];
		$data['additional_value'] = $values['additional_value'];
		$data['error_message_bank'] = $values['error_message_bank'];
		$data['error_code_bank'] = $values['error_code_bank'];
		$data['billing_address'] = $values['billing_address'];
		$data['billing_city'] = $values['billing_city'];
		$data['billing_country'] = $values['billing_country'];
		$data['shipping_address'] = $values['shipping_address'];
		$data['shipping_city'] = $values['shipping_city'];
		$data['shipping_country'] = $values['shipping_country'];
		$data['commision_pol'] = $values['commision_pol'];
		$data['commision_pol_currency'] = $values['commision_pol_currency'];
		$data['transaction_bank_id'] = $values['transaction_bank_id'];

		return $data;
		
	}
	
	/**
   *
	 * @author Gabriel Martinez
   **/
	function _validatePayment($values) {
		$result = array();
		$result['customer_notified'] = 1;
		
		// validate signature
		
		// if second decimal of value is zero we round to 1 decimal
		$val = number_format($values['value'], 1, '.', '');
		$signMd5 = $this->_getSignMd5($values, $val);
		
		if(strtoupper($values['sign']) != strtoupper($signMd5) ){
			// try with two
			$val = number_format($values['value'], 2, '.', '');
			$signMd5 = $this->_getSignMd5($values, $val);
			
			if(strtoupper($values['sign']) != strtoupper($signMd5) ) {
				return FALSE;
			}
		}
		
		// validate transaction state
		$trxState = $values['state_pol'];
		
		if($trxState == '6' || $trxState == '5' || $trxState == '104') {
			$result['order_status'] = $this->_currentMethod->payment_declined_status;
		} elseif($trxState == '4') {
			$result['order_status'] = $this->_currentMethod->payment_approved_status;
		} else {
			$result['order_status'] = $this->_currentMethod->payment_held_status;
		}
		
		$result['comments'] = '';
		
		return $result;
	}
	

	function _getSignMd5($values, $val) {
		return md5($this->_currentMethod->api_key . '~' . $values['merchant_id'] . '~' . $values['reference_sale'] . '~' .
			$val . '~' . $values['currency'] . '~' . $values['state_pol']);
	}

	function _getResponsePayuValues() {

		$fields = array(
    	"merchantId" => "usuario_id",
    	"merchant_name" => "usuario_nombre",
    	"merchant_address" => "usuario_direccion",
    	"telephone" => "telefono",
    	"merchant_url" => "usuario_url",
    	"transactionState" => "estado",
    	"lapTransactionState" => "estado_lap",
    	"message" => "mensaje",
    	"referenceCode" => "ref_venta",
    	"reference_pol" => "ref_pol",
    	"transactionId" => "transaction_id",
    	"description" => "descripcion",
    	"trazabilityCode" => "trazability_code",
    	"cus" => "cus",
    	"orderLanguage" => "idioma",
    	"extra1" => "extra1",
    	"extra2" => "extra2",
    	"extra3" => "extra3",
    	"signature" => "firma",
    	"polResponseCode" => "codigo_respuesta_pol",
    	"lapResponseCode" => "codigo_respuesta_lap",
    	"risk" => "riesgo",
    	"polPaymentMethod" => "medio_pago",
    	"lapPaymentMethod" => "medio_pago_lap",
    	"polPaymentMethodType" => "tipo_medio_pago",
    	"lapPaymentMethodType" => "tipo_medio_pago_lap",
    	"installmentsNumber" => "cuotas",
    	"TX_VALUE" => "valor",
    	"TX_TAX" => "iva",
    	"currency" => "moneda",
    	"lng" => "lng",
    	"pseCycle" => "ciclo_pse",
    	"buyerEmail" => "emailComprador",
    	"pseBank" => "banco_pse",
    	"pseReference1" => "pseReferencia1",
			"pseReference2" => "pseReferencia2",
    	"pseReference3" => "pseReferencia3",
    	"authorizationCode" => "codigo_autorizacion",
    	"TX_ADMINISTRATIVE_FEE" => "tarifa_administrativa",
    	"TX_TAX_ADMINISTRATIVE_FEE" => "iva_tarifa_administrativa",
    	"TX_TAX_ADMINISTRATIVE_FEE_RETURN_BASE" => "base_tarifa_administrativa",
    	"processingDate" => "fecha_procesamiento",
    	"cc_number" => "numero_visible",
    	"cc_holder" => "tarjeta_habiente",
    	"request_number" => "numero_pedido",
    	"processing_date_time" => "fecha_hora_procesamiento",
    	"action_code_description" => "descripcion_codigo_accion"
  	);
	
		$values = array();

  	foreach($fields as $key => $value) {
    	$val = ( isset($_REQUEST[$key]) )? $_REQUEST[$key] : $_REQUEST[$value];
    	if(isset($val)) {
      	$values[$key] = $val;
    	}
  	}

		return $values;

	}
	
	function _getConfirmationPayuValues() {

		$fields = array(
    	"nickname_buyer" => "nickname_comprador",
    	"nickname_seller" => "nickname_vendedor",
    	"transaction_id" => "transaccion_id",
    	"merchant_id" => "usuario_id",
    	"date" => "fecha",
    	"transaction_date" => "fecha_transaccion",
    	"payment_request_state" => "estado_transaccion",
    	"sign" => "firma",
    	"test" => "prueba",
    	"reference_sale" => "ref_venta",
    	"reference_pol" => "ref_pol",
    	"airline_code" => "codigo_aerolinea",
    	"description" => "descripcion",
    	"ip" => "ip",
    	"extra1" => "extra1",
    	"extra2" => "extra2",
    	"attempts" => "intentos",
			"currency" => "moneda",
    	"state_pol" => "estado_pol",
    	"email_buyer" => "email_comprador",
    	"exchange_rate" => "tasa_cambio",
    	"value" => "valor",
    	"additional_value" => "valorAdicional",
    	"tax" => "iva",
    	"error_message_bank" => "mensaje_error_banco",
    	"error_code_bank" => "codigo_error_banco",
    	"pse_reference1" => "pse_referencia1",
    	"pse_reference2" => "pse_referencia2",
			"pse_reference3" => "pse_referencia3",
    	"risk" => "riesgo",
    	"response_code_pol" => "codigo_respuesta_pol",
    	"response_message_pol" => "mensaje_respuesta_pol",
    	"billing_address" => "direccionCobro",
    	"billing_city" => "ciudadCobro",
    	"billing_country" => "paisCobro",
    	"shipping_address" => "direccionEnvio",
			"shipping_city" => "ciudadEnvio",
    	"shipping_country" => "paisEnvio",
    	"phone" => "telefono",
    	"office_phone" => "telefonoOficina",
    	"cus" => "cus",
    	"pse_bank" => "banco_pse",
    	"customer_number" => "numeroCliente",
    	"bank_id" => "banco_id",
    	"payment_method_id" => "medio_pago_id",
    	"payment_method" => "medio_pago",
    	"payment_method_type" => "tipo_medio_pago",
    	"installments_number" => "cuotas",
			"account_number_ach" => "numero_cuenta_deb_ach",
			"account_type_ach" => "tipo_cuenta_deb_ach",
			"bank_id_ach" => "banco_id_deb_ach",
			"administrative_fee" => "tarifa_administrativa",
			"administrative_fee_tax" => "iva_tarifa_administrativa",
			"administrative_fee_base" => "base_tarifa_administrativa",
			"commision_pol" => "comision_pol",
			"commision_pol_currency" => "moneda_comision_pol",
			"transaction_bank_id" => "transaccion_banco_id",
			"authorization_code" => "codigo_autorizacion"
  	);
	
		$values = array();

  	foreach($fields as $key => $value) {
    	$val = ( isset($_REQUEST[$key]) )? $_REQUEST[$key] : $_REQUEST[$value];
    	if(isset($val)) {
      	$values[$key] = $val;
    	}
  	}

		return $values;

	}

}
