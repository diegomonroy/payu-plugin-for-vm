<?php
/**
 * @version $Id: payulatam.php, v 1.0 2014/04/07
 *
 * Payulatam payment plugin
 *
 * @author Gabriel Martinez
 * @package VirtueMart
 * @subpackage payment
 * @copyright Copyright (C) 2014 Payu Latam - All rights reserved
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
 *
 * http://virtuemart.net
 */
defined('_JEXEC') or die();

$success = $viewData["success"];
$payment_name = $viewData["payment_name"];
$values = $viewData["values"];
$order = $viewData["order"];
$currency = $viewData["currency"];

?>
<br />
<table>
  <tr>
      <td><?php echo JText::_('VMPAYMENT_PAYULATAM_PAYMENT_NAME'); ?></td>
        <td><?php echo $payment_name; ?></td>
    </tr>

  <tr>
      <td><?php echo JText::_('COM_VIRTUEMART_ORDER_NUMBER'); ?></td>
        <td><?php echo $order['details']['BT']->order_number;; ?></td>
    </tr>
  <tr>
    <td><?php echo JText::_('VMPAYMENT_PAYULATAM_AMOUNT'); ?></td>
        <td><?php echo $values['TX_VALUE'] . ' ' . $values['currency']; ?></td>
    </tr>
  <tr>
    <td><?php echo JText::_('VMPAYMENT_PAYULATAM_STATUS'); ?></td>
        <td><?php echo $values['message']; ?></td>
    </tr>
  <tr>
      <td><?php echo JText::_('VMPAYMENT_PAYULATAM_TRANSACTION_ID'); ?></td>
        <td><?php echo $values['reference_pol']; ?></td>
    </tr>

</table>
	<br />
	<a class="vm-button-correct" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$viewData["order"]['details']['BT']->order_number.'&order_pass='.$viewData["order"]['details']['BT']->order_pass, false)?>"><?php echo JText::_('COM_VIRTUEMART_ORDER_VIEW_ORDER'); ?></a>
